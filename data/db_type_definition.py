import numpy as np
from scipy.ndimage import gaussian_filter
import datetime as dt
import pandas as pd
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import train_test_split
import pydotplus

from sklearn.pipeline import Pipeline
from sklearn.preprocessing import OneHotEncoder
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from pathlib import Path
from joblib import dump, load
import os

from shared.distance import tabular_feature_similarity_to_x
from shared.utils import apply_mask_image

from settings import *

cfg_img = cfg['base']['image']

ONE_HOT_ENCODE_PATH = "data/saved_models/one_hot_encoder_"
BASE_DIR = Path(__file__).resolve().parent.parent

class BaseXAI():
    """
    Abstract class for base class for specific data types.
    """
    def __init__(self):
        self.x_raw, self.y_true = self.load_data()
        self.model = self.load_model()
        self.y_raw = self.model(self.x_raw)
        self.scope_type = None
        self.encoded_feature_names = None
        self.categorical = None
        self.scope_pred = None
        self.regression = True
        self.classification = False

    def init_scope(self, how='random'):
        if how == 'all':
            self.scope = self.population
            self.scope_type = 'global'
        else:
            if type(how)==int:
                self.scope = self.population.loc[how:how]
            if how == 'random':
                self.scope = self.population.sample(1)
            self.scope_type = 'local'
            self.scope_pred = self.y_raw[self.scope.index[0]]


class TabularBase(BaseXAI):
    """
    Abstract class for tabular data.
    Contains methods to calculate distances, standard deviation and to handle categorical data.
    """
    def __init__(self):
        BaseXAI.__init__(self)
        self.data_type = 'tabular'
        self.population = self.x_raw.copy()
        # self.one_hot_encoder = self._train_one_hot_encoder()
        # self._train_and_save_model()
        self.std = self.compute_std()
        if getattr(self, "categorical_features", None):
            self.categorical = True
        else:
            self.categorical = False
            self.min_values = self.compute_min()
            self.max_values = self.compute_max()
        try:
            self.feature_names
        except:
            self.feature_names = [str(x) for x in self.x_raw.columns]
        self.one_hot_encoder = None

    def pop_similarity_to_x(self, x, features_subset=[]):
        dist_df = pd.DataFrame()
        # define the list of columns that are used to compute the similarity
        if len(features_subset) > 0:
            col_list = features_subset
        else:
            col_list = self.population.columns
        # compute column-wise similarity
        for col in col_list:
            dist_df[col] = tabular_feature_similarity_to_x(self.population[col],
                                                           x[col].values[0],
                                                           self.std.get(col, None))
        return dist_df.mean(axis=1)

    def compute_std(self):
        data_copy = self.x_raw.copy()
        for col in data_copy.columns:
            if type(data_copy[col].head(1).values[0]) in [dt.date]:
                data_copy.loc[:, col] = data_copy[col] - dt.date(2000, 1, 1)
        return data_copy.std()

    def compute_min(self):
        return self.x_raw.min()

    def compute_max(self):
        return self.x_raw.max()

    def load_one_hot_encoder(self):
        try:
            preprocessor = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
            if not preprocessor.get_params()['transformers'][0][2] == self.categorical_features:
                preprocessor = self._train_one_hot_encoder()
        except FileNotFoundError:
            preprocessor = self._train_one_hot_encoder()
        return preprocessor

    def _train_one_hot_encoder(self):
        # encoding categorical features for the decision tree
        non_cate_features = [x for x in self.feature_names if x not in self.categorical_features]
        categorical_transformer = Pipeline(steps=[('onehot', OneHotEncoder(handle_unknown='ignore'))])
        preprocessor = ColumnTransformer(
            transformers=[('cat', categorical_transformer, self.categorical_features)],
            remainder='passthrough',
            sparse_threshold=0,
        )
        preprocessor.fit(self.population[self.feature_names])
        dump(preprocessor, os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        return preprocessor

    def one_hot_encode(self, samples):
        """
        Apply learned one hot encoding. Need to account for samples that are subset of all columns.
        """
        if len(samples.columns) < len(self.feature_names):
            extra_cols = self.population[[x for x in self.feature_names if x not in samples.columns]]
            extra_cols = extra_cols.sample(len(samples)).copy()
            extra_cols.set_index(samples.index, inplace=True)
            full_col_samples = pd.concat([samples, extra_cols], axis=1)[self.feature_names]
            col_augmented = True
        else:
            col_augmented = False
            full_col_samples = samples[self.feature_names]
        X = self.one_hot_encoder.transform(full_col_samples)
        cate_fnames = self.one_hot_encoder.transformers_[0][1]['onehot'].get_feature_names()

        # put old column name in the encoded feature name
        cate_fname_full = [self.categorical_features[int(f.split('_')[0][1:])] + '__is__' + f[3:] for f in cate_fnames]
        non_cate_features = [x for x in self.feature_names if x not in self.categorical_features]
        col_names = list(cate_fname_full) + list(non_cate_features)
        encoded_samples = pd.DataFrame(data=X, columns=col_names)
        self.encoded_feature_names = encoded_samples.columns
        self.encoded_names_dict = {
            k: list(filter(lambda x: x.split('__')[0] == k, self.encoded_feature_names))
            for k in self.feature_names
        }
        if col_augmented:
            encoded_samples = encoded_samples[
                [x for x in encoded_samples.columns if x in samples.columns or x.split('__')[0] in samples.columns]
            ]
        return encoded_samples


class ImageBase(BaseXAI):
    """
    WIP

    Abstract class for image data.
    Contains pixel representation of image to allow sampling with generic methods.
    Basicaly, the image is represented as a vector of n_pix * n_pix big pixels (bpixel).
    Big pixels can be modified in different ways: bluring, masking.
    """
    def __init__(self):
        BaseXAI.__init__(self)
        self.data_type = 'image'
        self.n_pix = cfg_img['n_pixel']
        self.init_bpixel_representation(self.n_pix)
        self.feature_names = self.population.columns
        self.raw_model = self.load_model()
        self.model = self.representation_model()

    def init_bpixel_representation(self, n_pix=cfg_img['n_pixel'], which=cfg_img['representation']):
        if self.x_raw[0].shape[0] % n_pix != 0:
            raise ValueError("n_pix should be a divisor of " + str(self.x_raw[0].shape[0]))
        if which=='inversion':
            self.population = self.bpixel_inversion_representation(n_pix)
            self.representation_type = 'inversion'
        if which=='blur':
            self.population = self.bpixel_blur_representation(n_pix).astype(float)
            self.representation_type = 'blur'
        if which=='mask':
            self.population = self.bpixel_mask_representation(n_pix)
            self.representation_type = 'mask'
        self.std = self.set_std_to_one()
        self.feature_names = self.population.columns
        self.n_pix = n_pix
        try:
            self.update_scope()
        except:
            None

    def bpixel_inversion_representation(self, n_pix):
        image_representation = pd.DataFrame(index=range(len(self.x_raw)),
                                            columns=[str(i) for i in range(n_pix ** 2)]
                                            )
        image_representation.loc[:] = 1
        image_representation = image_representation.mul(range(len(self.x_raw)), axis=0)
        return image_representation

    def bpixel_blur_representation(self, n_pix):
        image_representation = pd.DataFrame(index=range(len(self.x_raw)),
                                            columns=[str(i) for i in range(n_pix ** 2)]
                                            )
        image_representation.loc[:] = float(0)
        return image_representation

    def bpixel_mask_representation(self, n_pix):
        image_representation = pd.DataFrame(index=range(len(self.x_raw)),
                                            columns=[str(i) for i in range(n_pix ** 2)]
                                            )
        image_representation.loc[:] = np.nan
        return image_representation

    def raw_from_representation(self, representations):
        new_images = []
        if self.representation_type=='inversion':
            for rpz in representations.iterrows():
                original_img_index = rpz[0]
                original_img = self.x_raw[original_img_index].copy()
                mask_indices = rpz[1][self.feature_names].astype(int)
                for mask_index in mask_indices.unique():
                    if original_img_index != mask_index:
                        mask_image = self.x_raw[mask_index]
                        mask = np.array((mask_index == mask_indices).values).reshape(self.n_pix, self.n_pix)
                        apply_mask_image(original_img, mask_image, mask)
                new_images.append(original_img)
        if self.representation_type=='blur':
            new_images = []
            for rpz in representations.iterrows():
                original_img_index = rpz[0]
                original_img = self.x_raw[original_img_index].copy()
                mask_sigma_list = rpz[1][self.feature_names]
                blured_images = {s: gaussian_filter(original_img, sigma=s) for s in mask_sigma_list.unique()}
                for sigma in mask_sigma_list.unique():
                    if sigma != 0:
                        mask_image = blured_images[sigma]
                        mask = np.array((mask_sigma_list == sigma).values).reshape(self.n_pix, self.n_pix)
                        apply_mask_image(original_img, mask_image, mask)
                new_images.append(original_img)
        if self.representation_type=='mask':
            new_images = []
            for rpz in representations.iterrows():
                original_img_index = rpz[0]
                original_img = self.x_raw[original_img_index].copy()
                mask_val_list = rpz[1][self.feature_names]
                for new_val in mask_val_list.unique():
                    if not np.isnan(new_val):
                        mask_image = np.ones(original_img.shape) * new_val
                        mask = np.array((mask_val_list==new_val).values).reshape(self.n_pix, self.n_pix)
                        apply_mask_image(original_img, mask_image, mask)
                new_images.append(original_img)
        return np.array(new_images)

    def update_scope(self, scope):
        self.scope = self.population.loc[scope.index]
        return self.scope

    def set_std_to_one(self):
        std = self.population.std(axis=0)
        std.loc[:] = 1
        return std

    def pop_similarity_to_x(self, x_rpz, features_subset=None):
        dist = np.linalg.norm(self.raw_from_representation(self.population) - self.x_raw[x_rpz.index[0]], axis=(1,2)).flatten()
        return 1. / (1 + dist)


class TextBase(BaseXAI):
    """
    WIP
    Abstract class for text data.
    Instanciate a word based representation of sentences to allow the use of generic sampling methods.
    """
    def __init__(self, **kwargs):
        BaseXAI.__init__(self)
        self.data_type = 'text'
        self.vocab_size = len(self.tokenizer.word_index) + 1
        self.init_representation()
        self.model = self.representation_model()
        self.reverse_word_map = dict(map(reversed, self.tokenizer.word_index.items()))

    def init_representation(self):
        sequences = self.tokenizer.texts_to_sequences(self.x_raw)
        padded_X = pad_sequences(sequences, padding='post', maxlen=self.max_sequence_size)
        self.population = pd.DataFrame(data=padded_X, columns=self.feature_names)

    def train_tokenizer(self, x_raw):
        t = Tokenizer()
        t.fit_on_texts(x_raw)
        return t

    def word_from_token(self, token):
        reverse_word_map = dict(map(reversed, self.tokenizer.word_index.items()))
        reverse_word_map[0] = ''
        return reverse_word_map[token]

    def raw_from_representation(self, representations):
        # Creating a reverse dictionary
        # Function takes a tokenized sentence and returns the words
        def sequence_to_text(list_of_indices):
            # Looking up words in dictionary
            words = [self.reverse_word_map.get(word_idx) for word_idx in list_of_indices if word_idx!=0]
            return (words)
        # Creating texts
        my_texts = list(map(sequence_to_text, representations[self.feature_names].values))
        return [' '.join(words_list) for words_list in my_texts]

    def pop_similarity_to_x(self, x, features_subset=None):
        # convert text to set of words and compute jaccard index
        set_serie = self.population.apply(lambda x: set(x).difference({0}), axis=1)
        return tabular_feature_similarity_to_x(set_serie, set(x.values[0]).difference({0}))
