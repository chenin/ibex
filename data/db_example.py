import pickle
from sklearn import *
from keras.datasets import mnist
from keras import models
from keras import backend as K
import keras
from sklearn.ensemble import RandomForestClassifier
from tensorflow.keras.layers import Embedding
from tensorflow.keras.models import Sequential as SequencialTf
from tensorflow.keras.layers import Dense as DenseTf
from tensorflow.keras.layers import LSTM
from keras.applications import vgg16
from keras.preprocessing.image import load_img
from keras.preprocessing.image import img_to_array
from keras.models import Sequential
from keras.layers import Dense
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.svm import SVC
import pylab as plt
from sklearn.metrics import accuracy_score

from data.db_type_definition import *

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

# tf.logging.set_verbosity(tf.logging.ERROR)


def show_tree(dt, features_name):
    dot_data = tree.export_graphviz(dt,
                                    feature_names=features_name,
                                    out_file=None,
                                    filled=True,
                                    rounded=True)
    graph = pydotplus.graph_from_dot_data(dot_data)
    graph.write_png('data/saved_models/decision_tree_adult.png')


def keras_nn():
    classifier = Sequential()
    classifier.add(Dense(activation="relu", input_dim=99, units=12, kernel_initializer="uniform"))
    classifier.add(Dense(activation="relu", units=12, kernel_initializer="uniform"))
    classifier.add(Dense(activation="sigmoid", units=1, kernel_initializer="uniform"))
    classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return classifier


# Tabular data examples
class IrisBase(TabularBase):
    """
    Example of "Base" class definition. The base is used as a layer between IBEX and the black-box model and its data.
    The class class should at least contains the methods : __init__, load_data and load_model.
    The class is a child class of the type based class of db_type_definition.
    """
    def __init__(self):
        TabularBase.__init__(self)

    def __str__(self):
        return "iris"

    def load_data(self):
        """
        Loads data associated with the black-box model.
        :return: input data (dataframe), target data (dataframe)
        """
        iris = datasets.load_iris()
        df = pd.DataFrame(iris.data)
        df.columns = iris.feature_names
        self.feature_names = iris.feature_names
        self.class_names = iris.target_names
        return df, iris.target

    def load_model(self):
        """
        Loads black-box model as a python function.
        :return: model to be inspected
        """
        # load toy model
        svm_classifier = SVC(C=0.000001, gamma=.1)
        svm_classifier.fit(self.x_raw, self.y_true)
        return lambda x: svm_classifier.predict(x[self.feature_names])

    def display(self, samples):
        print(samples)



class AdultBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'sex', 'native_country', 'marital_status',
            'relationship', #'education'
        ]
        self.feature_names = [
            'age', 'education_num', # 'education',
            'marital_status',
            'relationship', 'sex', 'hours_per_week',
            'native_country',
        ]
        super(AdultBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.class_names = ['<50k', '>=50k']

    def __str__(self):
        return "adult"

    def load_data(self):
        data = pd.read_csv("./data/dataset/adult.csv")
        data.drop('fnlwgt', axis=1, inplace=True)
        data.drop('race', axis=1, inplace=True)
        # preprocess
        data.replace('?', np.nan, inplace=True)
        data.dropna(inplace=True)
        data.replace(['Divorced', 'Married-AF-spouse',
                      'Married-civ-spouse', 'Married-spouse-absent',
                      'Never-married', 'Separated', 'Widowed'],
                     ['divorced', 'married', 'married', 'married',
                      'not married', 'not married', 'not married'], inplace=True)
        data.reset_index(drop=True, inplace=True)
        data['Income'] = data['Income'].apply(lambda x: x.replace('.', ''))

        del data['occupation'], data['workclass']

        # format output variable as a 0/1 output
        data['Income'] = (data['Income'] == '>50K').astype(int)
        x, y = data.drop('Income', axis=1), data['Income']

        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_model(self, model_path='./data/saved_models/decision_tree_adult.joblib'):
        # return lambda x: 1
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('tree', model)])
        return lambda x: pipe.predict(x[self.feature_names])

    def display(self, samples):
        print(samples)

    def _train_and_save_model(self, model_path='data/saved_models/decision_tree_adult.joblib'):
        X = self.one_hot_encode(self.population[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, self.y_true, test_size=.33)

        dt = DecisionTreeClassifier(max_leaf_nodes=20)
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))

        show_tree(dt, X.columns)
        dump(dt, model_path)


class GermanBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'purpose', 'credit_history', 'other_payment_plans',
            'property_magnitude', 'housing', 'checking_status', 'savings_status', 'employment',
            'job', 'own_telephone', 'personal_status', 'foreign_worker', 'other_parties',
        ]
        self.feature_names = [
            'credit_amount', 'duration', 'purpose', 'existing_credits', 'credit_history', 'other_payment_plans',
            'property_magnitude', 'housing', 'checking_status', 'savings_status', 'employment',
            'job', 'age', 'num_dependents', 'installment_commitment', 'own_telephone',
            'personal_status', 'foreign_worker', 'other_parties', 'residence_since',
            ]

        super(GermanBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.class_names = ['good', 'bad']

    def __str__(self):
        return "german"

    def load_data(self):
        data = pd.read_csv("./data/dataset/dataset_31_credit-g.csv")
        y = (data['class'] == 'good').astype(bool).copy()
        del data['class']
        return data[self.feature_names], y  # column ordering to match with the model

    def load_model(self, model_path='./data/saved_models/random_forest_german.joblib'):
        # return lambda x: 1
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('random_forest', model)])
        return lambda x: pipe.predict(x[self.feature_names])

    def display(self, samples):
        print(samples)

    def _train_and_save_model(self, model_path='data/saved_models/random_forest_german.joblib'):
        X = self.one_hot_encode(self.population[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, self.y_true, test_size=.33)

        dt = RandomForestClassifier(max_depth=4, n_estimators=200, class_weight='balanced')
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))

        dump(dt, model_path)


class Random2dBase(TabularBase):
    def __init__(self, size=500):
        self.size = size
        TabularBase.__init__(self)
        self.class_names = [0, 1]

    def __str__(self):
        return "rdm2d"

    def load_data(self, n_features=5):
        C = np.array([[0.4, 0.4], [.9, .7]])
        sigma = 0.1
        def proba_red(x1, x2):
            return sum(np.exp(-((center[0] - x1) ** 2 + (center[1] - x2) ** 2) / (2 * sigma ** 2)) for center in C)

        ab = np.random.multivariate_normal([.2, .8], cov=[[70, 50], [50, 70]], size=self.size)
        ab = (ab - ab.min()) / (ab.max() - ab.min())
        cde = np.random.rand(3 * self.size).reshape((self.size, 3))

        X = np.concatenate([ab, cde], axis=1)
        Y = [int(np.random.random() < proba_red(x[0], x[1])) for x in X]
        X = pd.DataFrame(data=X, columns=range(n_features))
        return X, Y

    def load_model(self):
        # # we chose a decision tree for simplicity
        dtc = DecisionTreeClassifier(random_state=0)
        parameters = {'max_depth': range(2, 15)}  # regularization parameter
        clf = GridSearchCV(dtc, parameters)
        clf.fit(self.x_raw, self.y_true)
        return lambda x: clf.best_estimator_.predict(x)

    def display(self, population, title=None):
        plot_step = 0.02
        fig = plt.figure(figsize=(7, 7))
        x_min, x_max = self.min_values[0], self.max_values[0]
        y_min, y_max = self.min_values[1], self.max_values[1]
        xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                             np.arange(y_min, y_max, plot_step))
        size = len(xx.ravel())
        Z = self.model(
            np.c_[xx.ravel(), yy.ravel(), np.random.random(size), np.random.random(size), np.random.random(size)])
        Z = Z.reshape(xx.shape)            # plt.tight_layout(h_pad=0.5, w_pad=0.5, pad=2.5)
        plt.contourf(xx, yy, Z, cmap=plt.cm.RdYlBu)

        for x_point in population.iterrows():
            plt.scatter(x_point[1][0], x_point[1][1], marker='+', s=200, color='k')
        plt.title(title)

    def display_sampling(self, samples, title=None):
        plot_step = 0.02
        fig = plt.figure(figsize=(7, 7))
        x_min, x_max = self.min_values[0], self.max_values[0]
        y_min, y_max = self.min_values[1], self.max_values[1]
        xx, yy = np.meshgrid(np.arange(x_min, x_max, plot_step),
                             np.arange(y_min, y_max, plot_step))
        size = len(xx.ravel())
        Z = self.model(
            np.c_[xx.ravel(), yy.ravel(), np.random.random(size), np.random.random(size), np.random.random(size)])
        Z = Z.reshape(xx.shape)            # plt.tight_layout(h_pad=0.5, w_pad=0.5, pad=2.5)
        plt.contourf(xx, yy, Z, cmap=plt.cm.RdYlBu)
        for x_point in samples.iterrows():
            plt.scatter(x_point[1][0], x_point[1][1], marker='+', s=200, color='k')

        plt.scatter(self.scope[0], self.scope[1], marker='+', s=500, color='w')
        plt.title(title)


# Image data examples
class ImageNetBase(ImageBase):
    def __init__(self):
        self.model_proba = self.load_model_proba()
        ImageBase.__init__(self)
        with open('./data/dataset/imagenet_classes.p', 'rb') as fp:
             imagenet_classes = pickle.load(fp)
        self.class_names = {int(k): v for k, v in imagenet_classes.items()}

    def __str__(self):
        return "imagenet"

    def process_image(self, image_path):
        original = load_img(image_path, target_size=(224, 224))
        numpy_image = img_to_array(original)
        return numpy_image

    def load_data(self):
        img_db = np.array(
            [self.process_image('./data/dataset/images_examples/dog.jpg'),
             self.process_image('./data/dataset/images_examples/cat.jpg'),
             self.process_image('./data/dataset/images_examples/panda.jpg'),
             self.process_image('./data/dataset/images_examples/orange.jpg')]
        ) / 255.
        return img_db, [0] * len(img_db)

    def representation_model(self):
        vgg_model = vgg16.VGG16(weights='imagenet')
        return lambda x: \
            np.array(np.argmax(
                vgg_model.predict(
                    vgg16.preprocess_input(
                        self.raw_from_representation(x.copy()) * 255.
                    )), axis=1))

    def load_model(self):
        vgg_model = vgg16.VGG16(weights='imagenet')
        return lambda x: \
            np.array(np.argmax(
                vgg_model.predict(
                    vgg16.preprocess_input(x.copy() * 255.)
                ), axis=1))

    def load_model_proba(self):
        vgg_model = vgg16.u(weights='imagenet')
        return lambda x: \
            vgg_model.predict(
                vgg16.preprocess_input(
                    self.raw_from_representation(x.copy()) * 255.
                ))

    def display(self, representations):
        images = self.raw_from_representation(representations)
        if len(images.shape) == 4:
            for im in images:
                self.display_image(im)
        else:
            self.display_image(images)

    def display_image(self, image):
        plt.imshow(np.uint8(image * 255.))
        plt.axis('off')
        plt.show()

    def display_rows(self, representations, nrows=3):
        images = self.raw_from_representation(representations)
        plt.tight_layout()
        for idx in range(len(representations)):
            plt.subplot(nrows, len(representations) / nrows, idx + 1)
            plt.imshow(np.uint8(images[idx] * 255.))
            plt.axis('off')
        plt.show()


class MnistBase(ImageBase):
    def __init__(self):
        self.model_proba = self.load_model_proba()
        ImageBase.__init__(self)
        self.class_names = [str(digit) for digit in range(10)]

    def __str__(self):
        return "mnist"

    def load_data(self):
        img_rows, img_cols = 28, 28
        num_classes = 10
        (x_train, y_train), (x_test, y_test) = mnist.load_data()
        if K.image_data_format() == 'channels_first':
            x_train = x_train.reshape(x_train.shape[0], 1, img_rows, img_cols)
            x_test = x_test.reshape(x_test.shape[0], 1, img_rows, img_cols)
            input_shape = (1, img_rows, img_cols)
        else:
            x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, 1)
            x_test = x_test.reshape(x_test.shape[0], img_rows, img_cols, 1)
            input_shape = (img_rows, img_cols, 1)

        x_train = x_train.astype('float32')
        x_test = x_test.astype('float32')
        x_train /= 255
        x_test /= 255

        # convert class vectors to binary class matrices
        y_train = keras.utils.to_categorical(y_train, num_classes)
        if cfg['debug']:
            return x_train[0:2000], y_train[0:2000]
        else:
            return x_train, y_train

    def representation_model(self):
        mnist_keras_model = models.load_model('./data/saved_models/mnist.h5')
        return lambda x: np.argmax(mnist_keras_model.predict(
            self.raw_from_representation(x)), axis=1)

    def load_model(self):
        mnist_keras_model = models.load_model('./data/saved_models/mnist.h5')
        return lambda x: np.argmax(mnist_keras_model.predict(x), axis=1)


    def load_model_proba(self):
        mnist_keras_model = models.load_model('./data/saved_models/mnist.h5')
        return lambda x: mnist_keras_model.predict(self.raw_from_representation(x))


    def display(self, representations):
        images = self.raw_from_representation(representations)
        if len(images.shape) == 4:
            for im in images:
                self.display_image(im)
        else:
            self.display_image(images)

    def display_image(self, image):
        plt.imshow(image.reshape((28, 28)), cmap='Greys',  vmin=0, vmax=1, interpolation='nearest')
        plt.axis('off')
        plt.show()

    def display_rows(self, representations, nrows=3):
        images = self.raw_from_representation(representations)
        plt.tight_layout()
        for idx in range(len(representations)):
            plt.subplot(nrows, len(representations) / nrows, idx + 1)
            plt.imshow(images[idx].reshape((28, 28)), cmap='Greys', vmin=0, vmax=1, interpolation='nearest')
            plt.axis('off')
        plt.show()


# text classification
class AirlineBase(TextBase):
    def __init__(self):
        self.feature_names = [str(idx) for idx in range(36)]
        super(AirlineBase, self).__init__()
        self.class_names = ['negative', 'neutral', 'positive']

    def __str__(self):
        return "airline"

    def load_data(self):
        data = pd.read_csv("./data/dataset/Tweets.csv")
        data = data[['text', 'airline_sentiment']]
        data['text'] = data['text'].apply(
            lambda row: ''.join(list(filter(lambda x: x not in """!"#$%&()*+,-./:;<=>?@[\\]^_`{|}~\t\n""", row))).lower()
        )

        data['airline_sentiment'] = data['airline_sentiment'].replace({'negative': 0, 'neutral': 1, 'positive': 2})
        y = data['airline_sentiment']
        self.tokenizer = self.train_tokenizer(data['text'].values)
        self.max_sequence_size = max([len(sample.split()) for sample in data['text'].values])
        return np.array(data['text'].values), y

    def preprocess_data(self, x_raw):
        sequences = self.tokenizer.texts_to_sequences(x_raw)
        padded_X = pad_sequences(sequences, padding='post', maxlen=self.max_sequence_size)
        return padded_X

    def load_model(self):
        vocab_size = 17353
        maxlen = 36
        embedding_layer = Embedding(input_dim=vocab_size, output_dim=100,
                                    input_length=maxlen, trainable=False)
        lstm_virgin = SequencialTf()
        lstm_virgin.add(embedding_layer)
        lstm_virgin.add(LSTM(256,
                             dropout=0.2,
                             recurrent_dropout=0.5))
        lstm_virgin.add(DenseTf(3, activation='softmax'))
        lstm_virgin.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['acc'])

        lstm_virgin.load_weights("./data/saved_models/airline_lstm_weights.h5")
        return lambda x: np.argmax(lstm_virgin.predict(self.preprocess_data(x)), axis=1)

    def representation_model(self):
        vocab_size = 17353
        maxlen = 36
        embedding_layer = Embedding(input_dim=vocab_size, output_dim=100,
                                    input_length=maxlen, trainable=False)
        lstm_virgin = SequencialTf()
        lstm_virgin.add(embedding_layer)
        lstm_virgin.add(LSTM(256,
                             dropout=0.2,
                             recurrent_dropout=0.5))
        lstm_virgin.add(DenseTf(3, activation='softmax'))
        lstm_virgin.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['acc'])

        lstm_virgin.load_weights("./data/saved_models/airline_lstm_weights.h5")
        return lambda x: np.argmax(lstm_virgin.predict(x[self.feature_names]), axis=1)

    def display(self, representations):
        print(self.raw_from_representation(representations))


class LoanBase(TabularBase):
    def __init__(self):
        self.categorical_features = [
            'employment_type'
        ]
        self.feature_names = [
            #'disbursed_amount',
            'asset_cost', 'contrib_percentage', 'employment_type',
            'pri_no_of_accts', 'pri_active_accts', 'pri_overdue_accts',
            'pri_current_balance', 'primary_instal_amt',
            'new_accts_in_last_six_months', 'delinquent_accts_in_last_six_months',
            'average_acct_age', 'credit_history_length', 'no_of_inquiries',
            'age',
            # 'pan_flag', 'voterid_flag',
        ]
        # write custom rules by increasing priority
        self.custom_rules = [
            {
            'name': 'règle du premier crédit',
            'description': "politique de favorisation de premier crédit pour les faibles montants",
            'input_conditions': [('pri_no_of_accts',  ' <= ', ' 0 '), ('asset_cost',  ' <= ', ' 55000 ')],
            'output_val': 1,
        },
            {
            'name': 'règle des deux défauts dans les 6 mois',
            'description': "politique de refus systèmatique pour les clients deux fois en défaut dans les six derniers mois",
            'input_conditions': [('delinquent_accts_in_last_six_months',  ' >= ', ' 2 ')],
            'output_val': 0,
        },
            {
            'name': 'discriminatory_rule',
            'description': " Règle discriminatoire à l'encontre des 60 ans et plus  ",
            'input_conditions': [('age',  ' >= ', ' 60 '), ],
            'output_val': 0,
        },]
        super(LoanBase, self).__init__()
        self.one_hot_encoder = self._train_one_hot_encoder()
        self.one_hot_encode(self.population)
        self.class_names = ['bad', 'good']
        self.decoded_model = self.load_decoded_model()
        self.std = self.compute_std()

    def __str__(self):
        return "loan"

    def load_data(self):
        df = pd.read_csv("./data/dataset/balanced_loan_data_ready.csv")
        x, y = df.drop('decision', axis=1), df['decision']
        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_data_(self):
        def correct_date(date_str):
            if int(date_str.split('-')[2]) < 20:
                return date_str[0:6] + '20' + date_str[-2:]
            else:
                return date_str[0:6] + '19' + date_str[-2:]

        df = pd.read_csv(
            "./data/dataset/train.csv",
        )

        df = df.drop([
            'UniqueID', 'branch_id', 'supplier_id', 'Current_pincode_ID',
            'State_ID', 'Employee_code_ID', 'manufacturer_id',
            'PRI.SANCTIONED.AMOUNT', 'PRI.DISBURSED.AMOUNT',
            'Aadhar_flag', 'MobileNo_Avl_Flag', 'Driving_flag', 'Passport_flag',
            'PAN_flag', 'VoterID_flag',
        ], axis=1)
        df.columns = df.columns.str.replace('.', '_').str.lower()
        df = df[[col for col in df.columns if 'sec_' not in col and '_cns_' not in col]]
        df.loc[:, 'date_of_birth'] = pd.to_datetime(df['date_of_birth'].apply(correct_date))
        df['age'] = (dt.datetime.now() - df.loc[:, 'date_of_birth']).apply(lambda x: int(x.days) / 365.25)
        del df['date_of_birth']
        df.loc[:, 'disbursaldate'] = pd.to_datetime(df['disbursaldate'])
        del df['disbursaldate']
        df.loc[:, 'employment_type'] = df['employment_type'].str.replace(' ', '_')
        df.loc[df['employment_type'].isnull(), 'employment_type'] = 'None'
        df['average_acct_age'] = df['average_acct_age'].apply(lambda x: x[:-3].split('yrs')).apply(
            lambda x: int(x[0]) + int(x[1]) / 12.)
        df['credit_history_length'] = df['credit_history_length'].apply(lambda x: x[:-3].split('yrs')).apply(
            lambda x: int(x[0]) + int(x[1]) / 12.)
        df.loc[:, 'contrib_percentage'] = 100 - df['ltv']
        del df['ltv']
        x, y = df.drop('loan_default', axis=1), 1 - df['loan_default']

        return x[self.feature_names], y  # redefine column ordering to match the model ordering

    def load_model(self, model_path='./data/saved_models/decision_tree_loan.joblib'):
        model = load(model_path)
        OHEncoder = load(os.path.join(BASE_DIR, ONE_HOT_ENCODE_PATH) + str(self) + ".joblib")
        pipe = Pipeline(steps=[('OHEncode', OHEncoder),
                               ('tree', model)])

        def model(x):
            pred = pipe.predict(x[self.feature_names])
            for rule in self.custom_rules:
                cdt = rule['input_conditions']
                mask = x.eval(' and '.join([''.join(predicate) for predicate in cdt]))
                pred[mask] = rule['output_val']
            return pred
        return model

    def load_decoded_model(self, model_path='./data/saved_models/decision_tree_loan.joblib'):
        model = load(model_path)
        pipe = Pipeline(steps=[('tree', model)])
        return lambda x: pipe.predict(x[self.encoded_feature_names])

    def display(self, samples):
        print(samples)

    def train_and_save_model_(self, model_path='data/saved_models/decision_tree_loan.joblib'):
        x, y = self.load_data_()
        X = self.one_hot_encode(x[self.feature_names])

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=.33)

        dt = DecisionTreeClassifier(max_leaf_nodes=20, class_weight='balanced')
        dt.fit(X_train, y_train)
        print("Accuracy score (test dataset): ", accuracy_score(y_test, dt.predict(X_test)))
        # keep only samples that are correctly predicted
        print("Dataset size: ", len(x))
        mask = (y == dt.predict(X))
        x = x.loc[mask]
        y = y[mask]
        print("Removing ", sum(1 - mask), "samples incorrectly predicted")
        print("Dataset size: ", len(x))
        # balance final dataset (len(accepted) = len(refused))
        x.loc[:, 'decision'] = y
        print("Size decision 1", sum(x['decision'] == 1), "Size decision 0", sum(x['decision'] == 0))
        remove_size = int((sum(x['decision'] == 1) - sum(x['decision'] == 0)) * .7)
        drop_indices = x[(x['decision'] == 1)].sample(remove_size).index
        x = x.drop(drop_indices)
        print("Removing ", remove_size, "decision 1 samples to balance the classes")
        print("Dataset size: ", len(x))
        # save final dataset
        x.to_csv("data/dataset/balanced_loan_data_ready.csv", index=False)
        show_tree(dt, X.columns)
        dump(dt, model_path)
