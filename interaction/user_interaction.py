# -*- coding: utf-8 -*-
import copy
from interaction.objectives_parsing import parse_base
from data.db_example import *

GENERIC_QUESTIONS = {
    'base_choice': {
        'question': """Choose a data set: \n\t- Iris flower database (I), \n\t- German credit (G), \n\t- Adult income (A), \n\t- Mnist (M), \n\t- ImageNet (Im), \n\t- Airline sentiment (Ar).\n""",
        'answers': {'I': 'iris', 'G': 'german', 'A': 'adult', 'M': 'mnist', 'Im': 'imagenet', 'Ar': 'airline'},
        'next': {'I': 'local_global', 'G': 'local_global', 'A': 'local_global', 'M': 'local_focus', 'Im': 'local_focus', 'Ar': 'local_focus'},
        'parameter_name': 'base',
        'type': 'choice'
    },
    "local_global": {
        'question': """ Are you interested in global (G) or local (L) explanations?\n""",
        'answers': {'G': 'global', 'L': 'local'},
        'next': {'G': 'profil_global', 'L': 'local_focus'},
        'parameter_name': 'focus',
        'type': 'choice'
    },
    "local_focus": {
        'question': """ What is your point of interest? Type an integer between 0 and {population_size}. \n""",
        'answers': {'any': 'none'},
        'next': {'any': 'profil_local'},
        'parameter_name': 'local_focus',
        'type': 'integer'
    },
    'profil_global': {
        'question': """How do you want to be considered by IBEX: \n\t- Technical Expert (TE), \n\t- Lay User (LU), \n\t- Domain Expert (DE), \n\t- AUditor '(AU)? \n""",
        'answers': {'TE': 'technical_expert','LU': 'lay_user', 'DE': 'domain_expert', 'AU': 'auditor'},
        'next': {'TE': "objective_TE", "LU": "objective_LU_DE_global", "DE": "objective_LU_DE_global", "EA": "objective_EA"},
        'parameter_name': 'profil',
        'type': 'choice'
    },
    'profil_local': {
        'question': """How do you want to be considered by IBEX: \n\t- Technical Expert (TE), \n\t- Lay User (LU), \n\t- Domain Expert (DE), \n\t- AUditor '(AU)? \n""",
        'answers': {'TE': 'technical_expert','LU': 'lay_user', 'DE': 'domain_expert', 'AU': 'auditor'},
        'next': {'TE': "objective_TE", "LU": "objective_LU_DE_local", "DE": "objective_LU_DE_local", "EA": "objective_EA"},
        'parameter_name': 'profil',
        'type': 'choice'
    },
    'objective_TE': {
        'question': """What is the objective of the explanation: \n\t- Improving the system (I), \n\t- increasing Trust (T)?\n""",
        'answers': {'I': 'improving', 'T': 'trust'},
        'next': {'I': "end", "T": "end"},
        'parameter_name': 'objective',
        'type': 'choice'
    },
    "objective_LU_DE_global": {
        'question': """What is the objective of the explanation: \n\t- increasing Truth (T), \n\t- Challenging (C)?\n""",
        'answers': {'T': 'trust', 'C': 'contesting'},
        'next': {'C': "end", "T": "end", "D": "actionability"},
        'parameter_name': 'objective',
        'type': 'choice'
    },
    "objective_LU_DE_local": {
        'question': """What is the objective of the explanation: \n\t- increasing Truth (T), \n\t- Challenging (C), \n\t- future Actions (A)?\n""",
        'answers': {'T': 'trust', 'C': 'contesting', 'D': 'decision'},
        'next': {'C': "end", "T": "end", "D": "actionability"},
        'parameter_name': 'objective',
        'type': 'choice'
    },
    "objective_EA": {
        'question': """What is the objective of the explanation: \n\t- increasing Truth (T), \n\t- Challenging (C)?\n""",
        'answers': {'T': 'trust', 'C': 'contesting'},
        'next': {'C': "end", "T": "end"},
        'parameter_name': 'objective',
        'type': 'choice'
    },
    "actionability": {
        'question': """ What are your actionable features? \n  
        Type list of feature's index separated by a comma (ex: 2,5,6,9). Possible features are:
          {list_of_features}\n""",
        'next': {'any': "end"},
        'parameter_name': 'actionability',
        'type': 'list'
    },
}

def ask_user(message):
    answer = str(input(message))
    return answer

def base_specific_questions(base):
    questions = copy.deepcopy(GENERIC_QUESTIONS)
    features_list = '\n'
    for it, feat in enumerate(base.feature_names):
        features_list += '\t - ' + feat + ' (' + str(it) + ') ' + '\n'
    questions["actionability"]['question'] = questions["actionability"]['question'].format(list_of_features=features_list)
    questions["local_focus"]['question'] = questions["local_focus"]['question'].format(population_size=len(base.population) - 1)
    return questions

def collect_answers(question_dict):
    print("""Please answer the following question. 
          Note that any question can be skipped by returning nothing, but the first one, which is mandatory. """)
    parameters = {}
    idx = 'base_choice'
    ongoing=True
    while ongoing:
        # stopping criteria
        try:
            q = question_dict[idx]
        except:
            ongoing = False
            break

        # question asking
        answer = ask_user(q['question'])

        # verification of the answer type and coherence
        if q['type']=='choice':
            if answer == '' and q['parameter_name'] != 'base':
                if q['parameter_name'] == 'focus':
                    parameters[q['parameter_name']] = 'global'
                    answer = 'G'
                if q['parameter_name'] == 'local_focus':
                    parameters[q['parameter_name']] = 0
                    answer = 0
                if q['parameter_name'] == 'profil':
                    parameters[q['parameter_name']] = 'lay_user'
                    answer = 'LU'
                if q['parameter_name'] == 'objective':
                    parameters[q['parameter_name']] = 'trust'
                    answer = 'T'
                if q['parameter_name'] == 'actionability':
                    parameters[q['parameter_name']] = []
                    answer = []
            elif answer not in q['answers'].keys():
                choices_txt = ' or '.join([str(a) for a in q['answers'].keys()])
                print("Invalid answer, the input should be " + choices_txt)
                continue
            else:
                parameters[q['parameter_name']] = q['answers'][answer]
        if q['type']=='integer':
            try:
                parameters[q['parameter_name']] = int(answer)
                answer = 'any'
            except ValueError:
                print("Invalid answer, the input should be an integer. ")
                continue
        if q['type']=='list':
            try:
                string_list = answer.split(',')
                try:
                    typed_list = [int(item) for item in string_list]
                except ValueError:
                    typed_list = string_list
                parameters[q['parameter_name']] = typed_list
                answer = 'any'
            except ValueError:
                print("Invalid answer, the input should be a list of element separated by commas. ")
                continue

        if q['parameter_name']=='base':
            question_dict = base_specific_questions(parse_base(parameters['base']))
            if parameters['base'] in ['airline', 'mnist', 'imagenet']: # no global explanations
                parameters['focus'] = 'local'
        idx = q['next'][answer]

    return parameters
