from data.db_example import *
from explanation.explanation_placeholder import *
from delivering.rule_representation import *

from settings import *

OBJ_TO_REQ = {
    "technical_expert": {
        "improving": {
            "realism": 1,
            "nature": True,
            "generality": [3, 2, 1],
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation, PearsonCoefExplanation, CFactualExplanation],
            "simplicity": [3, 2, 1],
        },
        "trust": {
            "generality": 3,
            "nature": True,
            "simplicity": [3, 2, 1],
            "realism": [3, 2, 1],
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation,
                     PearsonCoefExplanation, CFactualExplanation],
        },
    },
    "lay_user": {
        'trust': {
            "simplicity": 3,
            "generality": 3,
            "realism": 3,
            "nature": [False, True],
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation,
                     PearsonCoefExplanation, CFactualExplanation],
        },
        'contesting': {
            "simplicity": 3,
            "generality": [1, 2, 3],
            "realism": 1,
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation, PearsonCoefExplanation, CFactualExplanation],
            "nature": [False, True],
        },
        'decision': {
            "format": [CFactualExplanation],
            "simplicity": 3,
            "generality": 1,
            "realism": 2,
            "nature": False,
        },
    },
    "domain_expert": {
        'trust': {
            "generality": 3,
            "realism": 2,
            "simplicity": [3, 2, 1],
            "nature": [True, False],
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation, PearsonCoefExplanation, CFactualExplanation],
        },
        'contesting': {
            "generality": [1, 2, 3],
            "realism": 1,
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation,
                     PearsonCoefExplanation, CFactualExplanation],
            "nature": [False, True],
            "simplicity": [3, 2, 1],
        },
        'decision': {
            "format": [CFactualExplanation],
            "generality": 1,
            "realism": 2,
            "nature": False,
            "simplicity": [3, 2, 1],
        },
    },
    "auditor": {
        'trust': {
            "generality": 3,
            "realism": 3,
            "simplicity": [3, 2, 1],
            "nature": [True, False],
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation, PearsonCoefExplanation, CFactualExplanation],
        },
        'contesting': {
            "generality": [1, 2, 3],
            "realism": 1,
            "format": [RBMExplanation, DTExplanation, FImportanceExplanation, PartialDepExplanation,
                     PearsonCoefExplanation, CFactualExplanation],
            "nature": [True, False],
            "simplicity": [3, 2, 1],
        },
    },
}

def user_input_parsing(input_parameters):
    requirements = {}
    other_parameters = {}

    # base
    other_parameters['base'] = parse_base(input_parameters['base'])

    # focus
    if input_parameters['focus'] == 'global':
        other_parameters['base'].init_scope('all')
    elif (input_parameters['focus'] == 'local'
          and type(input_parameters['local_focus']) == int):
        try:
            other_parameters['base'].init_scope(input_parameters['local_focus'])
        except ValueError:
            raise ValueError(str(input_parameters['local_focus']) + " is not in the population range. ")
    else:
        raise ValueError("Focus defined by the user is not valid. ")

    # constraints: every feature not actionability is a constraint
    if input_parameters.get('actionability', None):
        features = other_parameters['base'].feature_names
        constraints_indices = [idx for idx in range(len(features)) if idx not in input_parameters['actionability']]
        constraints = list(np.array(features)[constraints_indices])
        other_parameters['constraints'] = constraints
        requirements['actionability'] = True
    else:
        requirements['actionability'] = False

    # setting requirement values from user-defined objectives
    req = OBJ_TO_REQ[input_parameters['profil']][input_parameters['objective']]
    for k, v in req.items():
        requirements[k] = v

    if input_parameters['focus'] == 'global':
        requirements['format'] = [t for t in requirements['format'] if t in [DTExplanation, PearsonCoefExplanation, PartialDepExplanation]]
        requirements['generality'] = 1
        requirements['realism'] = 1
    elif input_parameters['focus'] == 'local':
        requirements['format'] = [t for t in requirements['format'] if t in [CFactualExplanation, RBMExplanation, FImportanceExplanation]]

    return requirements, other_parameters


def parse_base(base_name):
    if base_name == 'iris':
        base = IrisBase()
    elif base_name == 'german':
        base = GermanBase()
    elif base_name == 'adult':
        base = AdultBase()
    elif base_name == 'mnist':
        base = MnistBase()
    elif base_name == 'imagenet':
        base = ImageNetBase()
    elif base_name == 'airline':
        base = AirlineBase()
    else:
        raise ValueError(base_name + " is not a valid base name. ")
    return base
