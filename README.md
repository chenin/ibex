# IBEX

PoC implementation of Interactive Black-box EXplanations (IBEX)

The project runs on python3 and have dependencies:

```pip3 install -r requirements.txt```

Some examples of use can be found in the notebook demo_regalia.ipynb 


If you use a virtual environment, jupyter notebook should be launched from it. The easiest solution is to use conda environments. 


```
conda create --name ibexenv 
conda activate ibexenv 
conda install --file requirements.txt
jupyter notebook
``` 

Contact: clement.henin@inria.fr 