import pandas as pd
import numpy as np
import datetime as dt


def tabular_feature_similarity_to_x(feature_serie, x, col_std=1):
    if feature_serie.dtype in ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist)
    if type(feature_serie.head(1).values[0]) in [dt.date]:
        # L1 norm
        dist = abs(feature_serie - x) / col_std
        return 1 / (1 + dist.astype(float))
    if type(feature_serie.head(1).values[0])==set:
        # Jaccard similarity
        return feature_serie.apply(lambda item: len(item.intersection(x)) / float(len(item.union(x))))
    else:
        # Goodall similarity
        eq_counts = sum(feature_serie==x)
        N_tot = len(feature_serie)
        return (feature_serie==x) * (eq_counts * (eq_counts - 1)) / (N_tot * (N_tot - 1))


def compute_similarity_to_x(population, x, data_std, features_subset=None):
    feature_dist_df = pd.DataFrame()
    try:
        X = population.drop('output', axis=1)
    except:
        X = population
    if features_subset:
        for col in features_subset:
            feature_dist_df[col] = compute_feature_similarity_to_x(X[col], x[col].values[0],
                                                                   data_std.get(col, None))
        return feature_dist_df.mean(axis=1)
    else:
        for col in X.columns:
            feature_dist_df[col] = compute_feature_similarity_to_x(X[col], x[col].values[0],
                                                                   data_std.get(col, None))
        return feature_dist_df.mean(axis=1)


def compute_distance_to_x(population, x, data_std, features_subset=None):
    try:
        X = population.drop('output', axis=1)
    except:
        X = population
    if features_subset:
        similarities = compute_similarity_to_x(X, x, features_subset, data_std)
    else:
        similarities = compute_similarity_to_x(X, x, X.columns, data_std)
    dist = similarities.apply(lambda x: 1./x - 1 if x!=0 else np.inf)
    return dist
