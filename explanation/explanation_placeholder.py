import itertools

from generation.rule_based_model import *
from generation.counterfactual import *
from generation.features_specific import *
from shared.utils import *
from delivering.f_specific_plot import *
from delivering.rule_representation import *
import explanation

from settings import *

TECHNICAL_PARAM = cfg['technical_params']


def tech_param_sampling(parameters):
    current = copy.deepcopy(TECHNICAL_PARAM[str(parameters['base'])])
    current = current["sampling_scope__" + parameters['sampling_scope']]
    return current[parameters['sampling'].__name__]


def tech_param_generation(parameters):
    current = copy.deepcopy(TECHNICAL_PARAM[str(parameters['base'])])
    current = current["n_items_max__" + parameters['n_items_max']]
    return current[parameters['format'].__name__]


class Explanation():
    def __init__(self, base, **kwargs):
        self.base = base
        # sampling
        self.n_samples = kwargs.get('n_samples', None)
        self.sampling = kwargs.get('sampling', None)
        self.sampling_scope = kwargs.get('sampling_scope', None)
        self.actionability = kwargs.get('actionability', None)
        self.realism = kwargs.get('realism', None)
        self.actionable_features = kwargs.get('actionable_features', None)
        # generation
        self.format = kwargs.get('format', None)
        self.n_items_max = kwargs.get('n_items_max', None)
        # explanation
        self.explanation_data = None
        self.generated_bool = False
        # requirements defintion
        self.simplicity = None
        self.generality = None
        self.nature = None

    def estimate_simplicity(self):
        n_items = self.n_items()
        small = TECHNICAL_PARAM[str(self.base)]["n_items_max__S"]['n_items_max']
        medium = TECHNICAL_PARAM[str(self.base)]["n_items_max__M"]['n_items_max']
        if n_items <= small:
            self.simplicity = 3
        elif n_items <= medium:
            self.simplicity = 2
        else:
            self.simplicity = 1

    def estimate_requirements(self, requirements_list):
        for req in requirements_list:
            if getattr(self, "estimate_" + req, None) is not None:
                func = getattr(self, "estimate_" + req)
                func()

    def requirements(self):
        return {
            "simplicity": self.simplicity,
            "actionability": self.actionability,
            "realism": self.realism,
            "format": self.format,
            "generality": self.generality,
            "nature": self.nature,
        }

    def react(self, more=None, less=None, actionable_features=None):
        if (more is None) + (less is None) + (actionable_features is None) < 2:
            raise ValueError("Only one requirement can be modified with react function. ")
        else:
            if more is not None:
                if getattr(self, more) == 3:
                    raise ValueError(more, "requirement is already at the maximum value")
                else:
                    setattr(self, more, getattr(self, more) + 1)
            if less is not None:
                if getattr(self, less) == 1:
                    raise ValueError(less, "requirement is already at the minimum value")
                else:
                    setattr(self, less, getattr(self, less) - 1)
            if actionable_features is not None:
                self.actionable_features = actionable_features
        self.define_new_requirements(self.requirements())


    def define_new_requirements(self, new_requirements):
        new_parameters = explanation.enumerate.enumerate_parameters(self.base, new_requirements)[0]
        for param in new_parameters.keys():
            setattr(self, param, new_parameters[param])
        self.compute()


class RBMExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(RBMExplanation, self).__init__(base, **kwargs)
        self.nature = True

    def __str__(self):
        return "RBMExplanation"

    def compute(self):
        if True:
        # try:
            sampling_param = tech_param_sampling(self.__dict__)
            if str(self.base)=='airline':
                sampling_param['text_bool_array'] = True
            generation_param = tech_param_generation(self.__dict__)
            samples = self.sampling(self.base, actionable_features=self.actionable_features, **sampling_param)
            rules, self.rbm_scope, self.rbm_precision = rule_based_model(self.base, samples, actionable_features=self.actionable_features, **generation_param)
            if len(rules) == 0:
                self.generated_bool = False
            else:
                self.explanation_data = rules
                self.generated_bool = True
        else:
        # except:
            self.generated_bool = False

    def n_items(self):
        if not self.generated_bool:
            raise ValueError(self, " computation is necessary before estimating n_items. ")
        return len(self.explanation_data)

    def estimate_generality(self):
        if str(self.base) == 'airline':
            words = [self.base.scope[predicate[0]].values[0] for predicate in self.decode_rules()]
            samples = self.base.population.loc[self.base.population.isin(words).sum(axis=1) >= len(words)]

            if len(samples) == 0:
                self.generality = 1
            else:
                samples['output'] = self.base.model(samples)
                tot = sum(samples['output'] == self.base.scope_pred)
                if tot > (len(samples) / 2):
                    self.generality = 3
                elif tot > (len(samples) / 3):
                    self.generality = 2
                else:
                    self.generality = 1
        else:
            rule_clf = self.explanation_data
            samples = select_closest(self.base, int(10))
            close_samples_005 = samples[self.base.feature_names]
            close_samples_005_pred = samples['output']
            samples = select_closest(self.base, 50)
            close_samples_01 = samples[self.base.feature_names]
            close_samples_01_pred = samples['output']
            rule_scope_005 = self.rule_predict(close_samples_005)
            rule_scope_005 = (close_samples_005_pred==rule_scope_005)
            rule_scope_01 = self.rule_predict(close_samples_01)
            rule_scope_01 = (close_samples_01_pred == rule_scope_01)
            self.generality = 1
            if sum(rule_scope_005) / float(len(close_samples_005)) > .8:
                self.generality = 2
            elif sum(rule_scope_01) / float(len(close_samples_01)) > .8:
                self.generality = 3

    def decode_rules(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        rules = self.explanation_data
        if self.base.categorical:
            feature_names = self.base.encoded_feature_names
        else:
            feature_names = self.base.feature_names
        predicate_list = []
        for predicate in rules:
            if ' > ' in predicate:
                feat, val = predicate.split(' > ')
                op = ' > '
            if ' <= ' in predicate:
                feat, val = predicate.split(' <= ')
                op = ' <= '
            # handling categorical values
            if '__is__' in feat:
                feat, val = feat.split('__is__')
                op = op.replace('>', "is").replace('<=', 'is not')
            predicate_list.append((feat, op, val))
        return predicate_list

    def deliver(self):
        if str(self.base) == 'airline':
            words = [self.base.word_from_token(self.base.scope[predicate[0]].values[0]) for predicate in
                     self.decode_rules()]
            str_rule = 'IF\n        ' + ' and '.join(words)
            str_rule += '\n        appear in the text'
            str_rule += '\nTHEN\n    output is ' + str(self.base.class_names[self.base.scope_pred])
            print(str_rule)
        else:
            predicate_list = self.decode_rules()
            str_rule_list = [''.join(rule_tupple) for rule_tupple in filter_redundant_rules(predicate_list)]
            str_rule = 'IF\n        ' + '\n    and '.join(str_rule_list)
            if self.base.classification:
                str_rule += '\nTHEN\n    output is ' + str(self.base.class_names[self.base.scope_pred])
            else:
                str_rule += '\nTHEN\n    output is around' + str(self.base.scope_pred)
            print(str_rule)

    def rule_predict(self, inputs):
        predicate_list = self.decode_rules()
        bool_predicate_col = []
        for pred in predicate_list:
            if pred[1] == ' is ':
                bool_predicate_col.append(inputs[pred[0]] == pred[2])
            elif pred[1] == ' is not ':
                bool_predicate_col.append(inputs[pred[0]] != pred[2])
            elif pred[1] == '>':
                bool_predicate_col.append(inputs[pred[0]] > pred[2])
            elif pred[1] == '<=':
                bool_predicate_col.append(inputs[pred[0]] <= pred[2])
        return np.product(bool_predicate_col, axis=0)

    def estimate_nature(self):
        self.nature = True


class CFactualExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(CFactualExplanation, self).__init__(base, **kwargs)
        self.generality = False
        self.nature = False

    def __str__(self):
        return "CFactualExplanation"

    def compute(self):
        try:
            sampling_param = tech_param_sampling(self.__dict__)
            sampling_param['actionable_features'] = self.actionable_features
            generation_param = tech_param_generation(self.__dict__)
            cfs = find_counterfactual(
                self.base, self.sampling, sampling_param, generation_param
            )
            self.explanation_data = cfs[-1]
            self.generated_bool = True
        except:
            self.generated_bool = False

    def n_items(self):
        if not self.generated_bool:
            raise ValueError(self, " computation is necessary before estimating n_items. ")
        n_items = len(self.explanation_data)
        return n_items

    def estimate_generality(self):
        current_pred = self.base.model(self.base.scope)[0]
        cf = self.explanation_data
        samples = select_closest(self.base, 10)
        close_samples_005 = samples[self.base.feature_names].copy()
        samples = select_closest(self.base, 50)
        close_samples_01 = samples[self.base.feature_names].copy()
        for k, v in cf.items():
            close_samples_005.loc[:, k] = v['new_val']
        close_samples_005['output'] = self.base.model(close_samples_005[self.base.feature_names])
        rule_scope_005 = (close_samples_005['output'] == current_pred)
        for k, v in cf.items():
            close_samples_01.loc[:, k] = v['new_val']
        close_samples_01['output'] = self.base.model(close_samples_01[self.base.feature_names])
        rule_scope_01 = (close_samples_005['output'] == current_pred)
        self.generality = 1
        if sum(rule_scope_005) / float(len(close_samples_005)) > .9:
            self.generality = 2
        elif sum(rule_scope_01) / float(len(close_samples_01)) > .9:
            self.generality = 3

    def deliver(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        if self.base.classification:
            current_pred = self.base.class_names[self.base.scope_pred]
        else:
            current_pred = self.base.scope_pred
        cf_diff = self.explanation_data
        cf = self.base.scope.replace({k: {v['old_val']: v['new_val']} for k, v in cf_diff.items()})
        if self.base.classification:
            new_pred = self.base.class_names[self.base.model(cf)[0]]
        else:
            new_pred = self.base.model(cf)[0]
        if self.base.data_type == 'image':
            print('Counterfactual image predicted as : ', new_pred)
            return self.base.display(cf)
        if str(self.base)=='airline':
            cf_diff = self.detokenized_cf()
        cf_str = 'Changing the following features: \n    '
        cf_str += ',\n    '.join([k + ' from ' + str(v['old_val']) + ' to ' + str(v['new_val']) for k, v in cf_diff.items()])
        cf_str += "\nwill reslut in changing the output of the model from " + current_pred + ' to ' + new_pred
        print(cf_str)

    def detokenized_cf(self):
        return {k: {'old_val': self.base.word_from_token(v['old_val']),
                    'new_val': self.base.word_from_token(v['new_val'])} for k, v in self.explanation_data.items()}

    def estimate_nature(self):
        self.nature = False


class FImportanceExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(FImportanceExplanation, self).__init__(base, **kwargs)
        self.generality = False
        self.nature = False

    def __str__(self):
        return "FImportanceExplanation"

    def compute(self):
        if True:
        # try:
            sampling_param = tech_param_sampling(self.__dict__)
            if str(self.base) == 'airline' and self.realism==2:
                sampling_param['text_bool_array'] = True
            generation_param = tech_param_generation(self.__dict__)
            samples = self.sampling(self.base, **sampling_param, actionable_features=self.actionable_features)
            coef = feature_importance(self.base, samples, **generation_param)
            self.explanation_data = {k: v for k, v in coef.items() if k != ''}
            self.generated_bool = True
            if all([x == 0 for x in self.explanation_data.values()]):
                self.generated_bool = False
        else:
        # except:
            self.generated_bool = False

    def n_items(self):
        assert self.generated_bool,  "Computation is necessary before estimating n_items."
        not_null_coef = len([val for val in self.explanation_data.values() if val != 0.])
        return not_null_coef

    def estimate_generality(self):
        sampling_param = tech_param_sampling(self.__dict__)
        generation_param = tech_param_generation(self.__dict__)
        samples = self.sampling(self.base, **sampling_param, actionable_features=self.actionable_features)
        coef_std = lasso_coef_std(self.base, samples, **generation_param)
        if str(self.base) == 'airline':
            token_words = [self.base.scope[k].values[0] for k, v in self.explanation_data.items() if abs(v) > 0]
            words = [self.base.word_from_token(self.base.scope[k].values[0]) for k, v in
                              self.explanation_data.items() if abs(v) > 0]
            coef_std = {self.base.word_from_token(self.base.scope[k].values[0]): v for k, v in coef_std.items()}
        self.generality = 1
        df = pd.DataFrame()
        if str(self.base) == 'airline':
            neighrbours = self.base.population.loc[self.base.population.isin(token_words).sum(axis=1) >= len(token_words)].head(20)
        else:
            neighrbours = select_closest(self.base, 20)
        for idx in neighrbours.index:
            # initiate new base with neighbour local scope
            temp_base = copy.deepcopy(self.base)
            temp_base.init_scope(idx)
            params = self.__dict__
            params = {k: v for k, v in params.items() if k not in ['base', 'generality']}
            params['base'] = temp_base
            params['generality'] = False
            cfe_neighbour = FImportanceExplanation(**params)
            cfe_neighbour.compute()
            if str(self.base) == 'airline':
                coef = {cfe_neighbour.base.word_from_token(cfe_neighbour.base.scope[k].values[0]): v for k, v in cfe_neighbour.explanation_data.items() if abs(v) > 0}
                coef = {k: v for k, v in coef.items() if k in words}
            else:
                coef = cfe_neighbour.explanation_data
            if coef is not None:
                df = df.append(coef, ignore_index=True)
        if str(self.base) == 'airline':
            if len(df) > 2:
                self.generality = 2
            if len(df) > 5:
                self.generality = 3
        else:
            for col in df.columns:
                # opposite coefficients when the neighbour sample has a different output
                df.loc[:, col] = (df[col] * ((neighrbours['output'] - .5) * 2).values)
                # compute the "error term" by removing the scope coefficient
                df.loc[:, col] = np.abs(df[col] - self.explanation_data.get(col, 0))
                # bool to know if the error term is smaller than 20 % of the real value
                df.loc[:, col] = (df[col] <= coef_std[col] * 5)
            if df.mean().mean() > .5:
                self.generality = 2
            if df.mean().mean() > .9:
                self.generality = 3

    def deliver(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        if self.base.data_type == 'image':
            coef_2d_image(self.explanation_data)
        if self.base.categorical:
            feat_selection = [k + '__' + list(v.values())[0] for k, v in
                              self.base.scope[self.base.categorical_features].to_dict().items()]
            feat_selection += [f for f in self.base.feature_names if f not in self.base.categorical_features]
        else:
            feat_selection = self.base.feature_names
        importance_selection = {k: v for k, v in self.explanation_data.items() if k in feat_selection if abs(v)>0}
        if str(self.base) == 'airline':
            importance_selection = {self.base.word_from_token(self.base.scope[k].values[0]): v for k, v in self.explanation_data.items() if abs(v)>0}
        plot_fimportances(importance_selection)

    def estimate_nature(self):
        self.nature = False

class DTExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(DTExplanation, self).__init__(base, **kwargs)

    def __str__(self):
        return "DTExplanation"

    def compute(self):
        generation_param = tech_param_generation(self.__dict__)
        samples = identity_sampling(self.base, actionable_features=self.actionable_features)
        self.realism = 3
        self.explanation_data = decision_tree(self.base, samples, **generation_param)
        self.generated_bool = True

    def eval_accuracy(self):
        samples = identity_sampling(self.base, actionable_features=self.actionable_features)
        if self.base.categorical:
            X = self.base.one_hot_encode(samples)
        else:
            X = samples
        self.accuracy = self.explanation_data.score(X, self.base.y_raw)


    def n_items(self):
        assert self.generated_bool, " computation is necessary before estimating n_items."
        return self.explanation_data.tree_.node_count

    def deliver(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        if len(self.actionable_features) > 0:
            non_cate_features = [x for x in self.base.feature_names if x not in self.base.categorical_features]
            ordered_col_names = list(self.base.categorical_features) + list(non_cate_features)
            ordered_act_features = [f for f in ordered_col_names if f in self.actionable_features]
            f_names = [self.base.encoded_names_dict[f] for f in ordered_act_features]
            f_names = [item for sublist in f_names for item in sublist]
        else:
            f_names = self.base.encoded_feature_names
        if self.base.classification:
            display_decision_tree(self.explanation_data,
                                  f_names,
                                  self.base.class_names)
        else:
            display_decision_tree(self.explanation_data,
                                  f_names)


    def estimate_nature(self):
        self.nature = True

class PearsonCoefExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(PearsonCoefExplanation, self).__init__(base, **kwargs)

    def __str__(self):
        return "PearsonCoefExplanation"

    def compute(self):
        samples = identity_sampling(self.base, actionable_features=self.actionable_features)
        self.realism = 3
        self.explanation_data = pearson_correlation(self.base, samples)
        self.generated_bool = True

    def n_items(self):
        assert self.generated_bool, "Computation is necessary before estimating n_items."
        count = 0
        for pc_class in self.explanation_data.values():
            not_null_coef = [val for val in pc_class.values() if val != 0.]
            count += len(not_null_coef)
        return count

    def deliver(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        for class_it in self.explanation_data.keys():
            plot_fimportances(self.explanation_data[class_it], title=class_it)

    def estimate_nature(self):
        self.nature = False


class PartialDepExplanation(Explanation):
    def __init__(self, base, **kwargs):
        super(PartialDepExplanation, self).__init__(base, **kwargs)

    def __str__(self):
        return "PartialDepExplanation"

    def compute(self):
        None
        self.generated_bool = False
        # pdp_params = tech_param_generation(self.__dict__)
        # self.explanation_data = partial_dependance(self.base, **pdp_params, actionable_features=self.actionable_features)
        # self.realism = 2
        # self.generated_bool = True

    def n_items(self):
        assert self.generated_bool, "Computation is necessary before estimating n_items."
        return len(self.explanation_data)

    def deliver(self):
        assert self.generated_bool, "Computation is necessary before delivering."
        for feature in self.base.feature_names:
            self.explanation_data[feature].plot(x=feature)

    def estimate_nature(self):
        self.nature = False
