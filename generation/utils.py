import pandas as pd
from sklearn.linear_model import Lasso
import operator

from settings import *

gen_cfg = cfg['generation']


def rule_symbol_to_operator(symbol):
    if symbol == 'is':
        return operator.eq
    elif symbol == 'is not':
        return operator.ne
    elif symbol == '>':
        return operator.gt
    elif symbol == '>=':
        return operator.ge
    elif symbol == '<':
        return operator.lt
    elif symbol == '<=':
        return operator.le
    else:
        raise ValueError("Symbol " + symbol + " is not unvalid. ")


class SamplingArguments():
    """
    Generic container for sampling arguments. It allows to store the arguments and defines a generic way of narrowing
    or broadening the scope of the sampling. These methods are done differently w.r.t to the name of sampling arguments.
    """
    def __init__(self, arg_dict):
        self.counter_narrow = 0
        self.counter_broaden = 0
        for k, v in arg_dict.items():
            setattr(self, k, v)

    def __str__(self):
        return str(self.__dict__)

    def arguments(self):
        args_but_counters = {}
        for key, value in self.__dict__.items():
            if key not in ['counter_narrow', 'counter_broaden']:
                args_but_counters[key] = value
        return args_but_counters

    def narrow(self):
        if 'sigma' in self.__dict__.keys():
            self.sigma *= .9
        if 'normal_sigma' in self.__dict__.keys():
            self.normal_sigma *= .9
        if 'share' in self.__dict__.keys():
            self.share *= .9
        if 'n_col_distrib' in self.__dict__.keys():
            if self.counter_narrow >= 10:
                if self.n_col_distrib == 'uniform':
                    self.n_col_distrib = 'normal'
        self.counter_narrow += 1

    def broaden(self):
        if 'sigma' in self.__dict__.keys():
            self.sigma *= 1.1
        if 'normal_sigma' in self.__dict__.keys():
            self.normal_sigma *= 1.1
        if 'share' in self.__dict__.keys():
            if self.share * 1.1 < 1: # otherwise error
                self.share *= 1.1
        if 'n_col_distrib' in self.__dict__.keys():
            if self.counter_broaden >= 10:
                if self.n_col_distrib == 'normal':
                    self.n_col_distrib = 'uniform'
        self.counter_broaden += 1

def lasso_coef_std(base, samples, alpha=gen_cfg['lasso_reg']['alpha']):
    df = pd.DataFrame()
    for _ in range(100):
        bs = samples.sample(int(len(samples)/1.1))
        X = bs.drop('output', axis=1)
        y = bs['output']
        if getattr(base, 'categorical_features', False):
            X = base.one_hot_encode(X)
        mod = Lasso(alpha=alpha, tol=0.01)
        mod.fit(X, y)
        coef_dict = {key: val for (key, val) in zip(X.columns, mod.coef_)}
        df = df.append(coef_dict, ignore_index=True)
    return df.std().to_dict()
