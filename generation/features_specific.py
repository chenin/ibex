import pickle
import numpy as np
import datetime as dt
from sklearn.preprocessing import StandardScaler
from pandas.api.types import is_datetime64_any_dtype as is_datetime
from settings import *
from sampling.perturbation import *
from generation.utils import *
import pdb

gen_cfg = cfg['generation']

def pearson_correlation(base, samples, kwargs_generation={}):
    """
        Compute the pearson correlation coefficient between one column and the output
    cf Iooss, B., Lemaître, P., 2015. A Review on Global Sensitivity Analysis Methods, in: Dellino, G., Meloni, C. (Eds.), Uncertainty Management in Simulation-Optimization of Complex Systems. Springer US, Boston, MA, pp. 101–122. https://doi.org/10.1007/978-1-4899-7547-8_5

    :param base: base XAI
    :param samples: dataframe of samples (with output column)
    :param kwargs_generation: empty
    :return: dictionary of pearson coefficients
    """
    p_coef = {}
    x, y = samples.drop(columns='output'), samples['output']
    if base.categorical:
        x = base.one_hot_encode(x)
    for class_it in y.unique():
        p_coef_one_class = {}
        y_class = (y == class_it)
        for col in x.columns:
            num = np.mean((x[col] - x[col].mean())
                      * (y_class - y_class.mean()))
            den = x[col].std() * y_class.std()
            p_coef_one_class[col] = (num / den)
        p_coef[base.class_names[class_it]] = p_coef_one_class
    return p_coef


def feature_importance(base, pred_scope, samples, alpha=gen_cfg['lasso_reg']['alpha'],
                     return_coef=False):
    """
    Compute the feature importance based of the lasso coefficients
    :param base: base XAI
    :param pred_scope: scope of the explanation
    :param samples: samples
    :param alpha: lasso regularization param
    :param return_coef: if True also return lasso coefficients
    :return: Feature importance computer with lasso coef
    """
    samples = samples.dropna()
    X = samples.drop('output', axis=1)
    y = samples['output']
    if getattr(base, 'categorical_features', False):
        X = base.one_hot_encode(X)
        scope = base.one_hot_encode(pred_scope)[X.columns]
        to_remove = []
        for k in X.columns:
            if '__is__' in k:
                feat, val = k.split('__is__')
                if pred_scope[feat].values[0] != val:
                    to_remove.append(k)
        X = X.loc[:, [x for x in X.columns if x not in to_remove]]
    else:
        scope = pred_scope
    mod = Lasso(alpha=alpha, tol=0.01)
    normalizer = StandardScaler()
    X_normalized = pd.DataFrame(data=normalizer.fit_transform(X), columns=X.columns)
    mod.fit(X_normalized, y)
    coef_dict = {key: val for (key, val) in zip(X.columns, mod.coef_)}
    scope_dist = (
        pd.DataFrame(data=normalizer.transform(scope[X.columns]), columns=X.columns) - X_normalized.mean()
    ).to_dict('records')[0]
    fi = {key: coef_dict[key] * scope_dist[key] for key in coef_dict.keys() if abs(coef_dict[key] * scope_dist[key]) > 0}
    if base.data_type == 'text':
        fi = {scope[k].values[0]: v for k, v in fi.items() if int(scope[k].values[0]) > 0}
    if return_coef:
        return fi, coef_dict
    else:
        return fi


def partial_dependance(base, nbins=20, actionable_features=[], coupled_var=[]):
    """
    Compute data for the partial dependance plot.
    :param base: Base XAI
    :param nbins: number of bins for the histogram
    :param actionable_features: list of actionable features
    :return: dict of dict : {key = col, value = {feature_value: average model output value, ... }}
    """
    if len(actionable_features) > 0:
        list_of_columns = actionable_features
    else:
        list_of_columns = base.feature_names
    res = {}
    for i, col in enumerate(list_of_columns):
        # Numeric types
        coupled_index = np.where([col in x for x in coupled_var])[0]
        col_values_list = []
        if len(coupled_index) > 0:
            tuple_col = coupled_var[coupled_index[0]]
            base.scope['idx'] = 1
            mean_and_val_df = base.scope.groupby(list(tuple_col), dropna=False).count()['idx'].to_frame()
            del base.scope['idx']
            mean_and_val_df['mean_val'] = np.nan
            for val in mean_and_val_df.index.values:
                col_values_dict = {col: val for col, val in zip(tuple_col, val)}
                mean_output_value = replace_fixed_col_fixed_values(
                    base=base,
                    col_val_dict=col_values_dict,
                )['output'].mean()
                if any([pd.isnull(x) for x in val]):
                    idx_list = [repr(x) == repr(val) for x in mean_and_val_df.index.values]
                    mean_and_val_df.loc[idx_list, "mean_val"] = mean_output_value
                else:
                    mean_and_val_df.loc[val, "mean_val"] = mean_output_value
            g = mean_and_val_df.groupby(col, dropna=False)
            mean_and_val_df['weights'] = mean_and_val_df.idx / g.idx.transform("sum")
            mean_and_val_df['weighted_val'] = mean_and_val_df['mean_val'] * mean_and_val_df['weights']
            mean_and_val_df = mean_and_val_df.groupby(col, dropna=False).sum()
            for row in mean_and_val_df.iterrows():
                col_values_list.append({'x': row[0], 'y': row[1]['weighted_val'], 'size': row[1]['idx']})
        else:
            if base.population[col].dtype in ['int16', 'int32', 'int64', 'float16', 'float32', 'float64']:
                bins_pdp = []
                if base.population[col].nunique() < nbins:
                    bins_pdp = base.population[col].unique()
                else:
                    for q in np.linspace(0, 1, nbins):
                        bins_pdp.append(base.population[col].quantile(q))
                bins_pdp = sorted(list(set(bins_pdp)))
                hist_value = np.histogram(base.population[col], bins=bins_pdp)[0]
            # date type
            elif is_datetime(base.population[col]):
                bin_size = (max(base.population[col]) - min(base.population[col])).days / float(nbins)
                bins_pdp = np.array([min(base.population[col]) + dt.timedelta(days=round((1 + x) * bin_size))
                                   for x in range(0, nbins)])
                float_val = ((base.population[col].dropna() - dt.datetime(2000, 1, 1, 0, 0, 0)).apply(lambda x: int(x.days))).copy()
                bins_range_float = list(map(lambda x: int(x.days), bins_pdp[~pd.isnull(bins_pdp)] - dt.datetime(2000, 1, 1, 0, 0, 0)))
                hist_value = np.histogram(float_val, bins=bins_range_float)[0]
            # not numeric types
            else:
                bins_pdp = base.population[col].unique()
                hist_value = base.population[col].value_counts()[bins_pdp].values
            for val, size in zip(bins_pdp, hist_value):
                mean_output_value = replace_fixed_col_fixed_values(
                    base=base,
                    col_val_dict={col: val}
                )['output'].mean()
                col_values_list.append({'x': val, 'y': mean_output_value, 'size': size})
        res[col] = col_values_list
    return {k: v for k, v in res.items() if k in list_of_columns}
