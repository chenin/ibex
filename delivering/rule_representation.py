import graphviz
# from sklearn.externals.six import StringIO
import pydot
import sklearn.tree as tree
import pylab as plt
from matplotlib import colors
import numpy as np
import matplotlib.patches as mpatches
import re
import pdb
from settings import *
from io import StringIO
from bokeh.plotting import figure
from bokeh.models import OpenURL, TapTool
from bokeh.layouts import gridplot, layout
from bokeh.embed import components
from bokeh.models import HBar, FactorRange, ColumnDataSource, DataTable, DateFormatter, TableColumn, Div
from bokeh.palettes import RdYlGn

del_cfg = cfg['delivering']


def identity(arg):
    return arg


def display_decision_tree(dt, feature_names, class_names=None, save_file=del_cfg["save_tree_file"]):
    dot_data = StringIO()
    if save_file:
        tree.export_graphviz(dt,
                             out_file=dot_data,
                             feature_names=feature_names,
                             class_names=class_names,
                             filled=True, rounded=True,
                             impurity=False)

        graph = pydot.graph_from_dot_data(dot_data.getvalue())
        graph[0].write_png(save_file)
    img = plt.imread(save_file)
    plt.figure(figsize=(img.shape[0] / 40., img.shape[1] / 40.))
    plt.imshow(img)
    plt.axis('off')
    plt.show()


def rbm_format_rule(rbm):
    rule = []
    for predicate in rbm[0]:
        if '__is__' in predicate:
            feat, val = predicate.split('__is__')
            if ' > ' in predicate:
                op = ' est '
            elif ' <= ' in predicate:
                op = " n'est pas "
            else:
                raise ValueError("rule ", predicate, " not well formated")
            val = val.replace(' > 0.5', '').replace(' <= 0.5', '')
            formated_pred = feat + op + val
        else:
            for op in [' <= ', ' > ', ' == ', ' est ', " n'est pas "]:
                if len(predicate.split(op)) > 1:
                    feat, val = predicate.split(op)
                    formated_pred = feat + op + val
        rule.append(formated_pred)
    formated_rule = "Si " + '\n et '.join(rule) + ", alors, dans un voisinage du périmètre, la sortie de l'algorithme est celle "
    formated_rule += "du périmètre avec une précision de " + str(round(rbm[2], 3)) + " sur " + str(round(rbm[1])) + " échantillons. "
    return formated_rule


def display_rules(rule):
    message = ""
    message += "RULE:\n"
    message += "Local model : If:\n\t" + '\n\tand '.join(rule[0].split(' and ')) + "\nthen the output of the model equals the output of the scope with "
    message += "a precision of " + str(rule[1]["precision"])
    print(message)

