import pylab as plt
import numpy as np
from bokeh.plotting import figure
from bokeh.models import OpenURL, TapTool
from bokeh.layouts import gridplot, layout
from bokeh.embed import components
from bokeh.models import HBar, FactorRange, ColumnDataSource, DataTable, DateFormatter, TableColumn, Div
from bokeh.palettes import RdYlGn

from settings import *

def plot_fimportances(imp_dict, title=None):
    """
    Create a pylab plot of features importances
    :param imp_dict: dict: key=column name, value : feature importance
    :return: pylab plot
    """
    imp_dict = {k: v for k, v in sorted(imp_dict.items(), key=lambda item: abs(item[1]))}
    plt.rcParams.update({'font.size': 21})
    fig = plt.figure(figsize=(10, len(imp_dict) * .8))
    y_pos = np.arange(len(imp_dict))
    clrs = ['b' if val>=0 else 'r' for val in imp_dict.values()]
    plt.barh(y_pos, list(imp_dict.values()), align='center', height=0.3, color=clrs)
    if title:
        plt.title(title)
    plt.yticks(y_pos, tuple(imp_dict.keys()))
    plt.savefig("fimportance.pdf", bbox_inches='tight')
    plt.show()

def coef_2d_image(base, imp_dict):
    n_pixels = base.n_pix
    imp_dict = {k: v for k, v in sorted(imp_dict.items(), key=lambda item: int(item[0]))}
    plt.imshow(np.array(list(imp_dict.values())).reshape(n_pixels, n_pixels))
    plt.colorbar()

def plot_partial_dependance(output_pdp):
    fig = plt.figure(figsize=(10, 3.5))
    for k, v in output_pdp.items():
        plt.plot(v[k], v.output)
        plt.xlabel(k)
        plt.ylabel("Mean output")
        plt.title(k)
        plt.show()


def plot_bokeh_pdp(base, pdp_data):
    val_plot = []
    dis_plot = []
    # figure probnp
    y_range_max = max([point['y'] for it in pdp_data.values() for point in it]) * 1.1
    ordered_features = pdp_data.keys()
    for feature in ordered_features:
        list_of_points = pdp_data[feature]
        # convert data for bokeh
        x = [point['x'] for point in list_of_points]
        y = [point['y'] for point in list_of_points]
        hist = np.array([point['size'] for point in list_of_points])
        hist = hist / sum(hist)
        if hasattr(base, 'categorical_features') and feature in base.categorical_features:
            x_sorted_label = [l for l, _ in sorted(zip(x, y), key=lambda it: it[1])]
            fig_val = figure(x_range=x_sorted_label,
                             title=feature,
                             y_range=(0, y_range_max),
                             height=300,
                             height_policy='fixed',
                             x_axis_label=feature,
                             y_axis_label="Sortie moyenne")
            hist_sorted_label = [l for l, _ in sorted(zip(hist, y), key=lambda it: it[1])]
            fig_val.vbar(x=x_sorted_label, top=sorted(y), width=0.9)
            # Setting the second y axis range name and range
            fig_dis = figure(x_range=fig_val.x_range, height=50, height_policy='fixed')
            fig_dis.vbar(x=x_sorted_label, top=hist_sorted_label, width=0.5, color='grey')
        else:
            fig_val = figure(
                 title=feature,
                 y_range=(0, y_range_max),
                 height=300,
                 height_policy='fixed',
                 x_axis_label=feature,
                 y_axis_label="Sortie moyenne")
            fig_val.line(x, y)
            bar_width = ((max(x) - min(x)) / len(x)) * .1
            fig_dis = figure(x_range=fig_val.x_range, height=50, height_policy='fixed')
            fig_dis.vbar(x=x, top=[1] * len(hist), width=bar_width, color='grey')
        fig_val.xaxis.major_tick_line_color = None
        fig_val.xaxis.minor_tick_line_color = None
        fig_val.xaxis.major_label_text_font_size = '0pt'
        fig_dis.yaxis.major_tick_line_color = None
        fig_dis.yaxis.minor_tick_line_color = None
        fig_dis.yaxis.major_label_text_font_size = '0pt'
        fig_dis.xgrid.grid_line_color = None
        fig_dis.ygrid.grid_line_color = None

        val_plot.append(fig_val)
        dis_plot.append(fig_dis)

    colonize_fig = np.array_split(val_plot, len(val_plot) // 3 + 1)
    colonize_dis = np.array_split(dis_plot, len(dis_plot) // 3 + 1)
    all_plots = []
    for i in range(len(colonize_dis)):
        all_plots.append(colonize_fig[i])
        all_plots.append(colonize_dis[i])

    return all_plots

def convert_feature_name(feature_name):
    if "__is__" in feature_name:
        feat, val = feature_name.split('__is__')
        return feat + ' est ' + val
    else:
        return feature_name


def fi_plot_bokeh(base, fi):
    orange = RdYlGn[3][2]
    green = RdYlGn[3][0]
    features_name_fi_plot = fi.keys()
    if base.data_type == 'text':
        features_name_fi_plot = [base.reverse_word_map[int(token)] for token in features_name_fi_plot]
    values_fi_plot = list(fi.values())
    values_fi_plot, features_name_fi_plot = zip(
        *sorted(zip(values_fi_plot, features_name_fi_plot)))  # sort by feature importance value
    colors = [orange if val < 0 else green for val in values_fi_plot]
    TOOLS = "save"
    # instantiating the figure object
    fi_plot = figure(title="Importance relative des variables",
                     y_range=FactorRange(factors=features_name_fi_plot),
                     plot_height=max(len(values_fi_plot) * 30, 200),
                     max_width=800,
                     tools=TOOLS,
                     align='center',
                     toolbar_location=None)
    source = ColumnDataSource(dict(y=features_name_fi_plot, right=values_fi_plot, color=colors))
    glyph = HBar(y="y", right="right", fill_color='color', left=0, height=0.3)
    fi_plot.add_glyph(source, glyph)
    fi_plot.xaxis.major_tick_line_color = None  # turn off x-axis major ticks
    fi_plot.xaxis.minor_tick_line_color = None  # turn off x-axis minor ticks
    fi_plot.xaxis.major_label_text_font_size = '0pt'  # preferred method for removing tick labels

    return fi_plot
