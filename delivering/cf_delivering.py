import pandas as pd
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import DataTable, TableColumn


def display_cf_differences(cf_list):
    all_text = ""
    for cf_num, counterfactual in enumerate(cf_list):
        all_text += "### Contrefactuelle " + str(cf_num + 1) + " ###\n"
        text = "Si "
        for it, (col, values) in enumerate(counterfactual.items()):
            if it == len(cf_list) - 1:
                text += col + " valait " + str(values['new_val']) + " et \n"
            else:
                text += col + " valait " + str(values['new_val']) + ", \n"
        text += " alors la sortie de l'algorithme serait différente. \n\n"
        all_text += text
    return all_text


def display_cf_table(cf_list, scope):
    # display counterfactuals as alternatives examples
    realistic_cf = scope.copy()
    for replace_dict in cf_list:
        modified_sample = scope.copy()
        for k, v in replace_dict.items():
            replace_dict[k] = {v['old_val']: v['new_val']}
        modified_sample = modified_sample.replace(replace_dict)
        realistic_cf = pd.concat([realistic_cf, modified_sample])

    # for col in realistic_cf.columns:
    #     realistic_cf.loc[:, col] = realistic_cf[col].apply(lambda x: format_feature_value(col, x))
    realistic_cf = realistic_cf.transpose()
    realistic_cf.reset_index(inplace=True)

    columns_label = [("var", "Variable"), ("file_val", "Dossier d'origine"), ]
    for it in range(len(cf_list)):
        columns_label.append(("cf_" + str(it + 1), "Contrefactuelle " + str(it + 1)))
    realistic_cf.columns = [c[0] for c in columns_label]

    table_data = realistic_cf.round(2).to_dict('series')
    table_data = {k: v.values for k, v in table_data.items()}

    source = ColumnDataSource(table_data)
    columns = [
        TableColumn(field=col[0], title=col[1], width=None) for col in columns_label
    ]
    rankings_table = DataTable(source=source, columns=columns,
                               autosize_mode="fit_columns", index_position=None,
                               sizing_mode="scale_width", )
    return rankings_table