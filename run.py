from explanation.enumerate import *
from interaction.user_interaction import *
from interaction.objectives_parsing import *

print("LIST OF QUESTIONS")
objectives = collect_answers(GENERIC_QUESTIONS)

requirements, other_param = user_input_parsing(objectives)

base = other_param['base']
print("COMPUTING THE EXPLANATION")
best_expl = select_explanation(base, requirements)

best_expl.deliver()